'use strict';

// Commands
// npm install --save-dev gulp-sass gulp-postcss autoprefixer cssnano gulp-sourcemaps

// Dependencies
var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps");

// SASS
var SassPaths = {
    styles: {
        src: "./wp-content/themes/phiservice/scss/**/*.scss",
        dest: "./wp-content/themes/phiservice/css"
    }
}

function style() {
    return (
        gulp
            .src(SassPaths.styles.src)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on("error", sass.logError)
            //.pipe(postcss([autoprefixer(), cssnano()]))
            .pipe(postcss([autoprefixer()]))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(SassPaths.styles.dest))
    );
}
 
exports.style = style;

// Watch
function watch(){
    gulp.watch(SassPaths.styles.src, style)
}

exports.watch = watch