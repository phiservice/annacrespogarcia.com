<?php
/**
 * Static Content Generator
 *
 * @link https://phiservice.com/
 * @package phiservice
 */

class StaticContent {

    /**
     * Constructor
     */
    public function __construct() {

        // Actions
        add_action( 'init', array( $this, 'register_static_content' ) );
        add_action( 'admin_notices', array( $this,'print_static_content_notice' ) );
        add_action( 'display_static_content_by_slug', array( $this, 'display_static_content_action' ), 10, 1);

        // Shortcodes
        add_shortcode( 'static_content', array( $this, 'display_static_content_shortcode' ) );
    }

    /**
     * Register the Static Content Post Type
     */
    public function register_static_content() {

        $labels = array(
            'name'                  => _x( 'Static contents', 'Post Type General Name', 'phiservice' ),
            'singular_name'         => _x( 'Static content', 'Post Type Singular Name', 'phiservice' ),
            'menu_name'             => __( 'Static content', 'phiservice' ),
            'name_admin_bar'        => __( 'Static content', 'phiservice' ),
            'archives'              => __( 'Static contents Archives', 'phiservice' ),
            'attributes'            => __( 'Static contents Attributes', 'phiservice' ),
            'parent_item_colon'     => __( 'Parent Static content:', 'phiservice' ),
            'all_items'             => __( 'All Static contents', 'phiservice' ),
            'add_new_item'          => __( 'Add New Static content', 'phiservice' ),
            'add_new'               => __( 'Add New', 'phiservice' ),
            'new_item'              => __( 'New Static content', 'phiservice' ),
            'edit_item'             => __( 'Edit Static content', 'phiservice' ),
            'update_item'           => __( 'Update Static content', 'phiservice' ),
            'view_item'             => __( 'View Static content', 'phiservice' ),
            'view_items'            => __( 'View Static content', 'phiservice' ),
            'search_items'          => __( 'Search Static content', 'phiservice' ),
            'not_found'             => __( 'Not found', 'phiservice' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'phiservice' ),
            'featured_image'        => __( 'Featured Image', 'phiservice' ),
            'set_featured_image'    => __( 'Set featured image', 'phiservice' ),
            'remove_featured_image' => __( 'Remove featured image', 'phiservice' ),
            'use_featured_image'    => __( 'Use as featured image', 'phiservice' ),
            'insert_into_item'      => __( 'Insert into item', 'phiservice' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Static content', 'phiservice' ),
            'items_list'            => __( 'Static content list', 'phiservice' ),
            'items_list_navigation' => __( 'Static content list navigation', 'phiservice' ),
            'filter_items_list'     => __( 'Filter Static content list', 'phiservice' ),
        );
    
        $args = array(
            'label'                 => __( 'Static content', 'phiservice' ),
            'description'           => __( 'Static content Generator', 'phiservice' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 20,
            'menu_icon'             => 'dashicons-align-center',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,		
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
    
        register_post_type( 'static_content', $args );
    }

    /**
     * Display notice info in your admin edit "Statics contents" page
     * 
     * @return html
     */
    public function print_static_content_notice(){
        $screen = get_current_screen();
        if( $screen->id !='edit-static_content' )
            return; 
        ?>    
        <div class="notice notice-warning">
          <p><?php echo __( 'Remember to use the "slug" of your "Static content" to call it inside in your shortcodes and actions.', 'phiservice' ); ?></p>
        </div>
       <?php 
    }

    /**
     * Dislpay Static Content Shortode
     * 
     * @param mixed $slug
     * @return html
     */
    public function display_static_content_shortcode($slug){
        return $this->get_content_by_slug($slug);
    }

    /**
     * Dislpay Static Content Action
     * 
     * @param mixed $slug
     * @return html
     */
    public function display_static_content_action($slug){
        echo $this->get_content_by_slug($slug);
    }

    /**
     * Get the static content by slug
     * 
     * @param mixed $slug
     * @return html
     */
    public function get_content_by_slug($slug){
        if(is_array($slug)){
            $slug = $slug['slug'];
        }
        
        $args = array(
            'name'        => $slug,
            'post_type'   => 'static_content',
            'numberposts' => 1
        );
        
        
        $post = get_posts($args);
        if($post){
            $html = apply_filters('the_content', $post[0]->post_content);
        } else {
            $html = 'There are no "Static Conten" available with this slug.';
        }

        return $html;
    }

}

$StaticContent = new StaticContent;