<?php
/**
 * Custom Content Generator
 *
 * @link https://phiservice.com/
 * @package phiservice
 */

class CustomContent {

    /**
     * Properties
     */
    private $customPostType;
    private $customPostTypeSlug;
    private $customTaxonomy;
    private $taxonomySlug;
    private $singularLabel;
    private $pluralLabel;

    /**
     * Constructor
     */
    public function __construct(
        $customPostType, 
        $customPostTypeSlug,
        $customTaxonomy,
        $taxonomySlug,
        $singularLabel,
        $pluralLabel
        ) {

        // Init Properties
        $this->customPostType       = $customPostType;
        $this->customPostTypeSlug   = $customPostTypeSlug;
        $this->customTaxonomy       = $customTaxonomy;
        $this->taxonomySlug         = $taxonomySlug;
        $this->singularLabel        = $singularLabel;
        $this->pluralLabel          = $pluralLabel;

        // Actions
        add_action( 'init', array( $this, 'register_custom_content' ));
        add_action( 'init', array( $this, 'register_custom_category' ));
        add_action( 'admin_notices', array( $this,'print_custom_content_notice' ) );
        add_action( 'display_custom_content_by_slug', array( $this, 'display_custom_content_by_slug' ), 10, 1);

        // Shortcodes
        add_shortcode( 'custom_content', array( $this, 'display_custom_content_by_slug' ) );

        // Filters
        add_filter( 'body_class', array( $this,'include_post_type_class' ), 10, 1 );
    }

    /**
     * Register the Custom Content Post Type
     */
    public function register_custom_content(){

        $labels = array(
            'name'                  => _x( $this->pluralLabel, 'Post Type General Name', 'phiservice' ),
            'singular_name'         => _x( $this->singularLabel, 'Post Type Singular Name', 'phiservice' ),
            'menu_name'             => __( $this->pluralLabel, 'phiservice' ),
            'name_admin_bar'        => __( $this->pluralLabel, 'phiservice' ),
            'archives'              => __( "$this->pluralLabel Archives", 'phiservice' ),
            'attributes'            => __( "$this->pluralLabel Attributes", 'phiservice' ),
            'parent_item_colon'     => __( "Parent $this->singularLabel:", 'phiservice' ),
            'all_items'             => __( "All $this->pluralLabel", 'phiservice' ),
            'add_new_item'          => __( "Add New $this->singularLabel", 'phiservice' ),
            'add_new'               => __( 'Add New', 'phiservice' ),
            'new_item'              => __( "New $this->singularLabel", 'phiservice' ),
            'edit_item'             => __( "Edit $this->singularLabel", 'phiservice' ),
            'update_item'           => __( "Update $this->singularLabel", 'phiservice' ),
            'view_item'             => __( "View $this->singularLabel", 'phiservice' ),
            'view_items'            => __( "View $this->singularLabel", 'phiservice' ),
            'search_items'          => __( "Search $this->singularLabel", 'phiservice' ),
            'not_found'             => __( 'Not found', 'phiservice' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'phiservice' ),
            'featured_image'        => __( 'Featured Image', 'phiservice' ),
            'set_featured_image'    => __( 'Set featured image', 'phiservice' ),
            'remove_featured_image' => __( 'Remove featured image', 'phiservice' ),
            'use_featured_image'    => __( 'Use as featured image', 'phiservice' ),
            'insert_into_item'      => __( 'Insert into item', 'phiservice' ),
            'uploaded_to_this_item' => __( "Uploaded to this $this->pluralLabel", 'phiservice' ),
            'items_list'            => __( "$this->pluralLabel list", 'phiservice' ),
            'items_list_navigation' => __( "$this->pluralLabel list navigation", 'phiservice' ),
            'filter_items_list'     => __( "Filter $this->pluralLabel list", 'phiservice' ),
        );
    
        $args = array(
            'label'                 => __( $this->singularLabel, 'phiservice' ),
            'description'           => __( "$this->singularLabel Generator", 'phiservice' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
            'taxonomies'            => array($this->customTaxonomy),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 20,
            'menu_icon'             => 'dashicons-welcome-learn-more',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'rewrite'	            => array('slug' => $this->customPostTypeSlug),
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
        );
    
        register_post_type( $this->customPostType, $args );
    }

    /**
     * Display notice info in your admin edit "Customs contents" page
     * 
     * @return html
     */
    public function print_custom_content_notice(){
        $screen = get_current_screen();
        if( $screen->id !="edit-$this->customPostType" )
            return; 
        ?>    
        <div class="notice notice-warning">
          <p><?php echo __( "Remember to use the 'slug' of your '$this->singularLabel' to call it inside in your shortcodes and actions.", 'phiservice' ); ?></p>
        </div>
       <?php 
    }

    /**
     * Dislpay Custom Content by Slug
     * 
     * @param mixed $slug
     * @return html
     */
    public function display_custom_content_by_slug($slug){

        if(is_array($slug)){
            $slug = $slug['slug'];
        }

        $args = array(
            'name'        => $slug,
            'post_type'   => $this->customPostType,
            'numberposts' => 1
        );
        
        $post = get_posts($args);
        if($post){
            $html = apply_filters('the_content', $post[0]->post_content);
        } else {
            $html = 'There are no "Custom Content" available with this slug.';
        }
        echo $html;
    }

    /**
     * Register Custom Categories Taxonomy
     */
    public function register_custom_category() {
     
        $labels = array(
            'name'              => _x( 'Categories', 'taxonomy general name' ),
            'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Categories' ),
            'all_items'         => __( 'All Categories' ),
            'parent_item'       => __( 'Parent Category' ),
            'parent_item_colon' => __( 'Parent Category:' ),
            'edit_item'         => __( 'Edit Category' ), 
            'update_item'       => __( 'Update Category' ),
            'add_new_item'      => __( 'Add New Category' ),
            'new_item_name'     => __( 'New Category Name' ),
            'menu_name'         => __( 'Categories' ),
        );    
     
        register_taxonomy($this->customTaxonomy,array($this->customPostType), array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => $this->taxonomySlug ),
        ));

    }

    public function include_post_type_class($classes){
        if (is_single() && $this->is_post_type($this->customPostType)){
            $classes[] = $this->customPostType;
        }
        return $classes;
    }

    public function is_post_type($type){
        global $wp_query;
        if($type == get_post_type($wp_query->post->ID)) 
            return true;
        return false;
    }
}

/**
 * Properties
 *
 * $customPostType;
 * $customPostTypeSlug;
 * $customTaxonomy;
 * $taxonomySlug;
 * $singularLabel;
 * $pluralLabel;
 **/

$customContent = new CustomContent(
    'obras', 
    'obras', 
    'categorias-obras', 
    'categorias-obras',
    __('Obra', 'phiservice'),
    __('Obras', 'phiservice')
);