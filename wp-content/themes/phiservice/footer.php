<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package phiservice
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer-top">
			<?php //do_action('display_static_content_by_slug', 'footer'); ?>
		</div>
		<div class="site-info">
			© <?php echo date("Y"); ?> www.annacrespogarcia.com
			<span class="sep"> | </span>
			<a href="<?php echo esc_url( __( 'https://annacrespogarcia.com/politica-privacidad/', 'phiservice' ) ); ?>">
				<?php echo __('Política de privacidad', 'phiservice'); ?>
			</a>
			<span class="sep"> | </span>
			<a href="mailto:contacto@annacrespogarcia.com">
				<?php echo __('Contacto', 'phiservice'); ?>
			</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
